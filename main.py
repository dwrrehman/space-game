import pygame
from opensimplex import OpenSimplex
import noise

background_colour = (255,255,255)

BLACK = (0, 0, 0)

dirt_color = (0x80, 0x80, 0x80)

rock_color = (155, 76, 0)

iron_color = (20,20,20)

calcite_color = (200, 200, 200)

window_height = 800
window_width = 800
block_size = 8 

block_rock = 1
block_dirt = 2
block_iron = 3
block_calcite = 4


planet = [[0 for i in range(0, 100)] for i in range(0, 100)]



def block_type(x):

	print(x)

	if x > 0.05:
		return block_iron

	if x < -0.05:
		return block_calcite

	if x > 0: 
		return block_dirt
	else:
		return block_rock

def generate_terrain():
	# os = OpenSimplex(seed=-1)

	seed = 42

	for x in range(0, 100):
		for y in range(0, 100):			
			# planet[x][y] = block_type(os.noise2d(x, y))
			planet[x][y] = block_type(noise.pnoise3(x / 100, y / 100, z / 100, octaves=10, base=seed))
	



def draw_terrain():

	

	px = 0

	for x in range(0, window_width, block_size):

		py = 0

		for y in range(0, window_height, block_size):
			
			color = (0,0,0)

		
			block = planet[px][py]

			if block == block_dirt:
				color = dirt_color

			if block == block_rock:
				color = rock_color

			if block == block_dirt:
				color = iron_color

			if block == block_calcite:
				color = calcite_color


			rect = pygame.Rect(x, y, block_size, block_size)

			pygame.draw.rect(screen, color, rect, 0)

			py += 1

		px += 1



pygame.init()

screen = pygame.display.set_mode((window_width, window_height))
pygame.display.set_caption('Space Game')
screen.fill(background_colour)
pygame.display.flip()

generate_terrain()

running = True

while running:
	# get keyboard input:
	for event in pygame.event.get():
		if event.type == pygame.QUIT or event.type == pygame.KEYDOWN and event.key == pygame.K_ESCAPE:
			running = False
		elif event.type == pygame.KEYDOWN:

			if event.key == pygame.K_r:
				print("you pressed r!")

	keystate = pygame.key.get_pressed()
	draw_terrain()	
	pygame.display.update()
