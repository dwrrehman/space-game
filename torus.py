import math
import matplotlib.pyplot as plt
# theta and phi are radians
def torus(R, r, theta, phi):
	
	x = R*math.cos(theta)+r*math.sin(phi)*math.cos(theta)
	y = R*math.sin(theta)+r*math.sin(phi)*math.sin(theta)
	z = r*math.cos(phi)
	return (x,y,z)

def graph():
	fig = plt.figure()
	ax = fig.add_subplot(projection='3d')
	xs = []
	ys = []
	zs = []
	for x in range(0,100):
		for y in range(0,100):
			tx,ty,tz = torus(100,50,((2*math.pi)/100)*x,((2*math.pi)/100)*y)
			xs.append(tx)
			ys.append(ty)
			zs.append(tz)
	ax.scatter(xs,ys,zs,s=1)
	plt.show()
graph()